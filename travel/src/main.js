import Vue from 'vue';
import App from './App';
import router from './router';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import store from './store'
import 'swiper/dist/css/swiper.css';
import fastClick from 'fastclick';
import './assets/styles/reset.css';
import './assets/styles/border.css';
import './assets/styles/iconfont.css';

Vue.config.productionTip = false;
fastClick.attach(document.body);
Vue.use(VueAwesomeSwiper);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components:{ App },
  render: h => h(App),
});
