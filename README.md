# Travel

#### 项目介绍
慕课网教程学习，[传送门](https://coding.imooc.com/class/203.html)  

一个纯前端项目，非常适合学习完 Vue2 基础，并且想要做项目实战的同学

项目运行地址：[传送门](https://www.gorit.cn/project/#/)

#### 安装教程

1.  git clone https://gitee.com/CodingGorit/Travel.git
2.  npm i
3.  npm run dev

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
